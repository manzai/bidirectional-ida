/* ===============================================================
   funzioni e dati specifici per il gioco del 15
   Versione velocizzata (modificata il 23-jan-92)
   =============================================================== */

#define psize 4  /* numero di bit per rappresentare una tessera */

/* ****** calcola la distanza di manhattan ***** */
int hdist(int a, int b)
{
#ifdef weight
  return weight*(abs((a%4) - (b%4)) + abs((a/4) - (b/4)));
#else
  return (abs((a%4) - (b%4)) + abs((a/4) - (b/4)));
#endif
}

/* ============ nuove posizioni del blank ======================== */
#if 0  
------------------- vecchia versione ---------------------
char right[pr_size] = {1, 2, 3,-1, 5, 6, 7,-1, 9,10,11,-1,13,14,15,-1};
char left[pr_size] = {-1, 0, 1, 2,-1, 4, 5, 6,-1, 8, 9,10,-1,12,13,14};
char up[pr_size] =   {-1,-1,-1,-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11};
char down[pr_size] =  {4, 5, 6, 7, 8, 9,10,11,12,13,14,15,-1,-1,-1,-1};
#endif


/* ====================================================================
   nuovapos[(dir<<psize) | p] da la nuova posizione di un pezzo mosso
   da p in direzione dir
   ==================================================================== */
char nuovapos[4*(1<<psize)]= {-1,-1,-1,-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,
                            1, 2, 3,-1, 5, 6, 7,-1, 9,10,11,-1,13,14,15,-1,
                            -1, 0, 1, 2,-1, 4, 5, 6,-1, 8, 9,10,-1,12,13,14,
                            4, 5, 6, 7, 8, 9,10,11,12,13,14,15,-1,-1,-1,-1};

