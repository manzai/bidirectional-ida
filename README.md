# Bidirectional IDA*

Implementation of the BIDA* algoritm used for the experiments in my old paper: [BIDA*: an improved perimeter search algorithm](https://doi.org/10.1016/0004-3702(95)00017-9). Included is also my implementation of Richard Korf [IDA* algorithm](https://doi.org/10.1016/0004-3702(85)90084-0). 

The two algorithms solve instances of the 15-puzzle. The C files contain the general algorithm while the `.h` files contain the parts specific to the 15-puzzle; by changing them it should be possibile to apply IDA* and BIDA* to other domains (but I never tried). 

This is the original 1995 code, with minimal changes to remove warnings and minimally check the command line arguments. The quality of the code is terrible (lot of global variables, goto's, and most comments are in Italian) but the output coincides with the 1995 experiments (except for the running times which are roughly 100 times smaller).


## Installation

Clone/download then `make`. This will create the executables `ida2` (IDA*) and `idanew11` (BIDA*).
Compile with `VERBOSE` set to 0 in the makefile to get rid of the per-iteration information.  


## Usage

Run program with no argument to get basic usage information. The input configuration is given on the command line with either an explicit description (16 integers) or two parameters for random generation (seed and number of random moves). 
The BIDA* algorithm requires as an additional parameter the depth of the bottom front.


## Additional files

The bash scripts `ida_test` and `bida_test` run IDA* and BIDA* on the 10 most difficult instances from the IDA* paper. The resulting output for my 2017 laptop is in the files `ida.log` and `bida.14.log`.  


## License

This code is available under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0)


