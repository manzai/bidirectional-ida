/* =================================================================
   funzioni per i giochi del 15, 19, 24 etc.
   comuni a tutti gli algoritmi di ricerca
   versione velocizzata 23-jan-92 (necessita di XXpuzzle.h)
   ================================================================= */


/* ============== posizione iniziale ==================== */
unsigned char start_t[pr_size];

/* ============================================================
   NB. deve essere stata gia' definita la variabile globale p0
   che contiene la posizione del blank
   ============================================================ */


/* **************************************************************
   input dello stato iniziale. Puo essere inserito dall'utente, oppure
   generato a caso facendo un certo numero di mosse.
   La posizione iniziale ottenuta e' messa in start_t[] e p0
   con la convenzione che start_t[i] e' la tile che si trova nella
   posizione i, e p0 e' la posizione del blank.
   ************************************************************** */
void get_start(int argc)
{
  void sprand(long);
  int randomint(int n);
  unsigned short x;        /* seme per i numeri casuali */
  int j,n,tile,new0;
  int dir, mossa_inversa;

  if(argc >= 1+pr_size) {
    // read initial configuration from command line
     for(j=0;j<pr_size; j++) {
      n = atoi(arglist[1+j]);
      start_t[j] = n;
      if(n==0) p0 = j;
    }
  }
  else if(argc>= 3) {
    // --- generate initial configuration with arglist[2] random moves
    x = atoi(arglist[1]);
    sprand(x);
    n = atoi(arglist[2]);
    for(j=0; j<pr_size; j++)  /* mette in start lo stato finale */
      start_t[j] = j;
    p0 = 0;
    mossa_inversa = -1;     /* inizialmente tutte le mosse sono valide */
    for( ; n>0;) {
      dir = randomint(4);            /* direzione del movimento */
      if(dir == mossa_inversa)
        continue;
      new0 = nuovapos[(dir<<psize)|p0];
      if(new0<0)
        continue;
      else {
        tile = start_t[new0];            /* tile mossa */
        start_t[new0] = 0;               /* nuova posizione del blank */
        start_t[p0] = tile;              /* nuova posizione di tile */
        p0 = new0;
        n--;
        mossa_inversa = invop(dir);
      }
    }
  }
  else {
    fprintf(stderr,"Invalid argument --get_start--\n");
    exit(1); 
  }
  printf("Initial configuration: ");
  for(j=0;j<pr_size; j++)
    printf("%3d", start_t[j]);
  printf("\n");
}


/* =================================================================
   tabella contenente le variazioni dell'euristica (vedi sotto)
   ================================================================= */
char manhvar[4*(1<<psize)*(1<<psize)];

/* ======= tabella mantab ========================= */
char mantab[pr_size][pr_size];

/* ***********************************************************************
   inizializza la tabella con la variazione della manhattan distance
   in manvar[dir][old0][j] c'e' la variazione dell'euristica per un pezzo
   destinato alla posizione j che si e' mosso da new0 con direzione invop(dir)
   (cioe' il blank si e' mosso da old0 a new0).
   Per motivi di efficenza si usa un array monodimensionale.
   ************************************************************************ */
void init_mantab(void)
{
  int dir,old0,j,k,new0, hdist(int, int);

  for(old0=0; old0<pr_size; old0++)     /* old0 = casella di partenza */
    for(dir=0; dir<4; dir++) {          /* dir = direzione mossa */
      k = (dir<<psize) | old0;          /* rappresenta nuovapos[dir][old0] */
      new0 = nuovapos[k];               /* casella di arrivo */
      if(new0==-1) continue;
      k = k<<psize;
      for(j=0;j<pr_size;j++)          /* destinazione finale del pezzo */
        manhvar[k|j] = hdist(old0,j) - hdist(new0,j); /*manhvar[dir][old0][j]*/
    }

  for(k=0; k<pr_size; k++)       /* i = numero del pezzo */
    for(j=0; j<pr_size; j++)     /* j =  numero della posizione */
  mantab[k][j] = hdist(k,j);  
}


/* ***************************************************
   generatore numeri casuali portatile
   *************************************************** */

/* =============================================================
   variabili globali per il generatore di numeri casuali
   ============================================================= */
#define PRANDMAX 1000000000L   /* massimo numero casuale */
long arr[55];
int axx,bxx;

long lprand()
{
  long t;

  if(axx-- == 0)
    axx=54;
  if(bxx-- == 0)
    bxx=54;
  t = arr[axx] - arr[bxx];
  if(t<0)
    t+= PRANDMAX;
  arr[axx] = t;
  return t;
}

void sprand(long seed)
{
  int i, ii;
  long last, next;

  if(seed<0)
    seed = -seed;
  seed = seed % PRANDMAX;
  arr[0]=last=seed;
  next=1;
  for(i=1;i<55;i++) {
    ii = (21*i) % 55;
    arr[ii] = next;
    next = last - next;
    if(next<0)
      next += PRANDMAX;
    last = arr[ii];
  }
  axx = 0;
  bxx = 24;
  for(i=0;i<165;i++)
    last = lprand();
}


/* *******************************************************************
   genera un numero casuale intero compreso fra 0 e n-1
   ******************************************************************* */
int randomint(int n)
{
  double f;
  int x;

  f = ((double) lprand())/PRANDMAX;
  x = (int) floor(f*n);
  assert((x>=0)&&(x<n));
  return x;
}

