/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   ida2.c: ida* algorithm implementation 
   ver 1.0  27-12-91
   ver 2.0  15-01-92 recursive search routine trasformed to iterative
   ver 2.1  ??-01-92 use of common.h
   ver 2.2  07-12-22 minor updates for XXI century compilers
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/* ############### compilation constants ############### */
#define pr_size 16           /* problem size */
/* ######################################################## */

// modern way to define clocks x seconds
// Note: on modern machines clock_t is a long (8 bytes)
// and CLOCKS_PER_SEC is 10^6 so wrap around should not
// be a provlem since 2^62 mu sec is about 10^5 years  
#define CLK_TCK (CLOCKS_PER_SEC)



/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   definizione della struttura nodo
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
typedef struct {
  unsigned char t[pr_size];  /* t[i] e' la tessera nella posizione i */
  unsigned char d;           /* distanza dal goal */
} nodo;



/* ================ ugly global variables ============================== */
/* int mantab[pr_size][pr_size]; (definito in commomfast.h) */
nodo current;      /* posizione corrente */
int p0;            /* posizione del blank */
int cur_h;         /* stima della distanza dal goal */
int bound;         /* limite per la ricerca */
unsigned long ngen;/* numero dei nodi generati */
int finito;        /* variabile booleana =1 se ha trovato la soluzione */
char **arglist;    /* lista degli argomenti */
/*  ================================================================= */

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   gli operatori vanno definiti in modo che -1 non sia un operatore
   e che la macro invop qui definita dia l'inverso di un operatore
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
#define invop(N) 3 - N

#if (pr_size==16)
#include "15puzzle.h"   /* legge le funzioni specifiche del gioco del 15 */
#endif
#include "common.h"    /* legge funzioni comuni a tutti gli alg. di ricerca*/

static void usage_and_exit(char *name)
{
    fprintf(stderr,"Usage:\n\t%s <initial state>\n",name);
    fprintf(stderr,"or:\n\t%s seed nm\n\n",name);
    fprintf(stderr,"In the second usage the initial configuration is generated executing\n");
    fprintf(stderr,"nm random moves.\n\n");
    fprintf(stderr,"Parameter seed can be used to generate the same instances even\n");
    fprintf(stderr,"on different architectures.\n\n");
    exit(1);
}


int main(int argc, char *argv[])
{
  void sprand(long);
  void init_current(void);  /* inizializza current */
  void init_mantab(void);   /* inizializza mantab */
  void search(void);        /* esegue la ricerca di una sol < bound */
  clock_t start,end;
  double total_time, delta_t;

  if(argc!=1+pr_size && argc!=3) 
    usage_and_exit(argv[0]);

  sprand(817459);    /* inizializza i numeri casuali */
  arglist =argv;
  init_mantab();   /* inizializza mantab */
  get_start(argc);     /* chiede all'utente la posizione iniziale */

  finito=0;
  ngen = 0;
  total_time = 0;
  start = clock();
  init_current();
  printf("Initial heuristic estimate: %d\n",cur_h);
  for(bound = cur_h; ; bound +=2 ) {
    end=clock();
    delta_t= ((double) (end-start))/(CLK_TCK);
    total_time += delta_t;
    start=clock();
    #if verbose
    fprintf(stdout,"bound: %3d, #nodes: %lu", bound, ngen);
    fprintf(stdout,"   CPU time: %lf seconds\n",total_time);
    #endif
    search();          
    if (finito) break;
  }
  end = clock();
  delta_t= ((double) (end-start))/(CLK_TCK);
  total_time += delta_t;
  printf("Total CPU time as reported by clock():");
  printf(" %lf seconds\n",total_time);
  return 0;
}


/* ****************************************************************
   cerca in maniera depth-first una soluzione di costo minore di "bound"
   assume che lo stato attuale sia stato ottenuto con un operatore il
   cui inverso e' "mossa"
   **************************************************************** */

#if 0
====================== recursive version ====================
void search(int mossa)
{
  void search(int);
  int move(int dir);
  int dir;               /* direzione del movimento */
  int new0, old0, tile,delta;

  old0 = p0;                 /* memorizza la posizione del blank */
  current.d++;               /* aumenta la distanza dallo stato iniziale */
  for(dir=0;dir < 4; dir++) {
    if(dir==mossa)
      continue;
    new0 = move(dir);
    if(new0==-1)
      continue;                /* direzione non valida */
    ngen++;                    /* nuovo nodo generato */
    tile = current.t[new0];    /* tile mossa */
    delta = mantab[tile][old0] - mantab[tile][new0];
    cur_h += delta;        /* nuova stima */
    if(current.d + cur_h <= bound) {
      current.t[new0] = 0;       /* nuova posizione del blank */
      current.t[old0] = tile;    /* nuova posizione di tile */
      p0 = new0;

      if(cur_h == 0) {             /* controlla se ha finito */
        printf("Solution found: costo %3d.  ",current.d);
        printf("#nodes generated: %lu\n", ngen);
        finito=1;
        return;
      }

      search(invop(dir));
      if(finito) return;

      current.t[old0] = 0;
      current.t[new0] = tile;
      p0 = old0;
    }
    cur_h -= delta;
  }
  current.d--;
}
#endif


/* **** stacks used to emulate recursion ******************** */
#define max_depth 200   /* massima profondita' di ricorsione */
char oldh[max_depth];
char old0[max_depth];
char dir_s[max_depth];
char new0_s[max_depth];
char tile_s[max_depth];
char mossa_s[max_depth];
int b, mossa_danonfare;


void search(void)
{
  int move(int dir);
  int dir;               /* direzione del movimento */
  int new0, tile,newh;
  int indice;

  mossa_danonfare = -1;   /* all'inizio bisogna fare tutte le mosse */
  b = bound-1;
  if(b>=max_depth) {
    fprintf(stderr,"Recursion depth exceeded --search() ---\n");
    exit(1);
  }
  
inizio:
  mossa_s[b] = mossa_danonfare;
  old0[b] = p0;                  /* memorizza la posizione del blank */
  oldh[b] = cur_h;               /* memorizza l'euristica */

  for(dir=0;dir<4; dir++) {
    if(dir==mossa_danonfare)
      continue;
    indice = (dir<<psize) | p0;
    new0 = nuovapos[ indice ];  /* sta per nuovapos[dir][p0]  */
    if(new0==-1)
      continue;                      /* direzione non valida */
    ngen++;                          /* nuovo nodo generato */
    tile = current.t[new0];    /* tile mossa */
    newh = cur_h + manhvar[(indice<<psize)|tile];  /* nuova versione */

    /* newh = cur_h + (mantab[tile][p0] - mantab[tile][new0]); vecchia ver */

    if(newh <= b) {
      if(newh == 0) {                /* controlla se ha finito */
        printf("Solution found: cost %3d.  ",bound);
        printf("#nodes generated: %lu\n", ngen);
        finito=1;
        return;
      }
      /* ------------- aggiorna la posizione ------------ */ 
      current.t[new0] = 0;       /* nuova posizione del blank */
      current.t[p0] = tile;      /* nuova posizione di tile */
      p0 = new0;
      cur_h = newh;
      /* --------------- salva le variabili ------------- */
      dir_s[b] = dir;
      tile_s[b] = tile;   
      new0_s[b] = new0;
      /* --------------- lancia iterazione a livello + basso --------- */
      mossa_danonfare = invop(dir);
      b--;
      goto inizio;
riprendi:
      /* -------------- ripristina le variabili che servono ----- */
      p0 = old0[b];
      cur_h = oldh[b];
      dir = dir_s[b];
      mossa_danonfare = mossa_s[b];
      /* -------------- ripristina la situazione ----------------- */
      current.t[p0] = 0;
      current.t[(int) new0_s[b]] = tile_s[b];
    }
  }
  if(++b<bound)
    goto riprendi;
}

/* ******************************************************************** */




/* **************************************************************
   inizializzazione del nodo current
   ************************************************************** */
void init_current(void)
{
  int j, tile;

  for(j=0;j<pr_size;j++)        /* copia lo stato iniziale in current */
    current.t[j] = start_t[j];

  current.d = 0;                /* azzera la distanza */
  cur_h = 0;                    /* azzera la stima */
  for(j=0; j<pr_size; j++) {
    tile = current.t[j];  /* tile nella posizione j */
    if (tile == 0)
      continue;
    cur_h += mantab[tile][j];      /* calcola l'euristica */
  }
}

