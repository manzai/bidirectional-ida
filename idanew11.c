/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   idanew11.c impelementation of the BIDA* algorithm
   Ver 1.0     22-nov-95
   Ver 1.1     07-dec-22 (minor updates for XXI century compilers)
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

/* ######################## compilation constants ####################### */
// ## Size of the main tables, increase if you need a deeper bottom front
#define max_size (16*8*65000)          // sizes increased 16x wrt 1995 code 
#define stack_size (16*2300000)       
#define hash_size (16*16*65536)
// ## do not change these constants 
#define pr_size 16                     // problem size (requires appropriate .h)
#define illegal_node max_size+1
#define weight 1         /* peso dato alla funzione f */
#define small_front 0    /* numero nodi bottom_front da tenere 0=tutti */
/* ######################################################################## */


/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   Dato che la hash table deve contenere tutti i nodi e' opportuno che sia
   hash_size = 2* max_size o piu'. Nella hash table non sono memorizzati
   direttamente i nodi, ma degli indici di posizioni nell'array nodi[].
   Di conseguenza il tipo index deve essere scelto in modo che possa
   rappresentare i valori da 0 a max_size-1 e illegal_node.
   Il tipo ht_index deve essere unsigned e poter rappresentare i numeri
   da 0 a hash_size
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
typedef unsigned int index;
typedef unsigned int ht_index;


// modern way to define clocks x seconds
// Note: on modern machines clock_t is a long (8 bytes)
// and CLOCKS_PER_SEC is 10^6 so wrap around should not
// be a provlem since 2^62 mu sec is about 10^5 years  
#define CLK_TCK (CLOCKS_PER_SEC)

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   gli operatori vanno definiti in modo che -1 non sia un operatore
   e che la macro invop qui definita dia l'inverso di un operatore
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
#define invop(N) (3 - N)

/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   definizione della struttura nodo
   >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> */
typedef struct {
  unsigned char p[pr_size];  /* p[i] e' la posizione della tessera i */
  char invers;          /* inverso dell'operatore che ha generato il nodo */
} nodo;

typedef nodo *nodopt;        /* puntatore a un nodo */


/* ================ ugly global variables ============================== */
nodo *nodi;              /* array dei nodi open */
index totale_nodi;       /* numero totale nodi in nodi[] */
nodopt *stack;           /* array contenente i nodi attivi */
unsigned char *hvalues;  /* h[i] e' l'euristica per il nodo stack[i] */
unsigned long ngen;/* numero di nodi espansi */
int t[pr_size];    /* tessere del nodo corrente */
int p0;            /* posizione del blank del nodo corrente */
int soglia;        /* massimo valore ammesso per l'euristica */
int bound;         /* limite per la ricerca */
int finito;        /* variabile booleana, 1= soluzione trovata */
int top_depth, bottom_depth;  /* profondita' fronti */
index top_begin, top_end; /* inizio e fine dei nodi top in nodi[] */
index bottom_begin, bottom_end; /* inizio e fine dei nodi bottom in nodi[] */
index bottom_start;              /* posizione del nodo soluzione in nodi[] */
index totale_bottom;             /* totale nodi nel bottom front */
index genera_begin, genera_end;  /* inizio e fine dell'output di genera_new*/
unsigned long heval;             /* numero di valutazioni della h */
char **arglist;                  /* lista degli argomenti */
int costo_soluzione;
/*  ================================================================= */

#if pr_size==16
#include "15puzzle.h" /* legge le funzioni specifiche del gioco del 15 */
#else
#include "24puzzle.h"
#endif
#include "common.h"  /* legge funzioni comuni a tutti gli alg. di ricerca*/


static void usage_and_exit(char *name)
{
    fprintf(stderr,"Usage:\n\t%s <initial state> bottom_front_depth\n",name);
    fprintf(stderr,"or:\n\t%s seed nm bottom_front_depth\n\n",name);
    fprintf(stderr,"In the second usage the initial configuration is generated executing\n");
    fprintf(stderr,"nm random moves.\n\n");
    fprintf(stderr,"Parameter seed can be used to generate the same instances even\n");
    fprintf(stderr,"on different architectures.\n\n");
    fprintf(stderr,"Parameter bottom_front_depth must be non-negative and is limited\n");
    fprintf(stderr,"in practice by the size of the algorithm tables.\n\n");    
    exit(1);
}


int main(int argc, char *argv[])
{
  void sprand(long);
  void alloc_mem(void);     /* inizializza la memoria */
  int init_search(void);    /* inizializza la ricerca */
  void init_mantab(void);   /* inizializza mantab */
  void mono_search(void);   /* esegue la ricerca di una sol <= bound */
  clock_t start,end;
  int start_h, step;
  
  if(argc!=2+pr_size && argc!=4) 
    usage_and_exit(argv[0]);

  sprand(817459);    /* inizializza i numeri casuali */
  arglist = argv;    /* copia gli argomenti in arglist */
  
  /* --------------- input dati ---------------------- */
  get_start(argc);     /* chiede all'utente la posizione iniziale */
  top_depth=0;
  if(argc!=4)
    bottom_depth = atoi(arglist[1+pr_size]);
  else 
    bottom_depth = atoi(arglist[3]);
  printf("Bottom front depth: %d\n", bottom_depth);
  if(bottom_depth<0) {
    fprintf(stderr,"The depth of the bottom front must be non-negative!\n");
    exit(1);
  }

  start = clock();
  init_mantab();   /* inizializza man_tab */
  alloc_mem();     /* alloca la memoria per tutti gli array necessari */
 
  ngen = 0;
  heval = 0;
  start_h = init_search();  /* genera i fronti e inizializza */
  #if (weight==1)
  step=2;
  #else
  step=1;
  #endif

  finito = 0;
  for(bound = start_h; ; bound += step) {
    end=clock();
    #if verbose
    fprintf(stdout,"bound: %3d, #nodes: %lu", bound, ngen);
    fprintf(stdout,"   CPU time: %lf seconds\n",((double) (end-start))/(CLK_TCK));
    #endif
    mono_search();
    if (finito) break;
  }
  end = clock();
  printf("Total CPU time as reported by clock():");
  printf(" %lf seconds\n",((double) (end-start))/(CLK_TCK));
  printf("Total # evaluations heuristic function: %lu\n",heval);
  return 0;
}

/* ********************************************************************
   Esegue la ricerca monodirizionale.
   Cerca una soluzione di costo <= bound.
   ******************************************************************** */
void mono_search(void)
{
  void search(int mossa,int,int);    /* esegue la ricerca depth-first */
  nodo n;
  int i;

  n=nodi[top_begin];            /* nodo da esplorare (= nodo iniziale)*/
  /* ------------- mette in t[] e p0 il nodo iniziale -------- */
  p0=n.p[0];
  for(i=0;i<pr_size;i++)
    t[n.p[i]] = i;
  /* ----------- esegue la ricerca ---------------- */
  soglia = bound-top_depth-bottom_depth;
  search(n.invers,0,totale_bottom);
}

 
/* ****************************************************************
   cerca in maniera depth-first una soluzione di costo <= "soglia"
   assume che lo stato attuale sia stato ottenuto con un operatore il
   cui inverso e' "mossa".
   I nodi attivi sono quelli nelle posizioni fra start e end-1 (compresi)
   **************************************************************** */
void search(int mossa, int start, int end)
{
  void search(int,int,int);
  int dir;               /* direzione del movimento */
  int new0, old0, tile, indice;
  register int newh;
  register nodopt pn;
  char *manh_offset;
  unsigned char *h_end,*h_start;
  nodopt *n_start, *n_end;
  register unsigned char *h_address;
  register nodopt *n_address;
  register unsigned char *h_good;/* euristica nodi che hanno superato la soglia*/
  register nodopt *n_good; /* puntatori ai nodi cha hanno superato la soglia*/
  register int counter;
  int nodi_attivi, nodi_sopravissuti;
 
  if(end>stack_size) {
    fprintf(stderr,"The array stack[] is full! --search()--\n");
    exit(1);
  }
  nodi_attivi = end-start;  /* numero di nodi attivi */
  h_start = hvalues + start; /* zona di hvalues[] contenente le euristiche */
  h_end = hvalues + end;
  n_start = stack + start;   /* zona di stack[] contenente i nodi */
  n_end = stack + end;
 
  old0 = p0;                 /* memorizza la posizione del blank */
  soglia--;                   /* aumenta la distanza dallo stato iniziale */
  for(dir=0;dir < 4; dir++) {
    if(dir==mossa)
      continue;
    indice = (dir<<psize) | p0;
    new0 = nuovapos[indice];        /* sta per nuovapos[dir][p0]  */
    if(new0<0)
      continue;                /* direzione non valida */
    ngen++;                    /* incrementa il numero dei nodi generati */
    tile = t[new0];            /* tile mossa */
    indice = indice<<psize;    /* prepara indice per la lettura di manh_var*/
    manh_offset = manhvar + indice; /* vedi definizione di manhvar */
 
    /* ---------------------------------------------------------------------
       copia gli indirizzi e l'euristica dei nodi la cui h e'< di "soglia"
       --------------------------------------------------------------------- */
    n_address = n_start;
    h_address = h_start;
    h_good = h_end;
    n_good = n_end;
    counter = nodi_attivi;

    heval += counter;   /* number of heuristics evaluation */
    do {
      pn = *(n_address++);
      newh = *(h_address++) + manh_offset[ pn->p[tile] ];
      if(newh <= soglia) {
        if(newh==0) {
          printf("Solution found: cost %3d.  ",bound-soglia);
          printf("#nodes generated: %lu\n", ngen);
          finito=1;
          costo_soluzione = bound-soglia;
          return;
        }
        *(n_good++) = pn;
        *(h_good++) = newh;
      }
    } while(--counter);
    nodi_sopravissuti = h_good - h_end;
    if(nodi_sopravissuti!=0) {   /* ------ continua la ricerca ---- */
      t[new0] = 0;               /* nuova posizione del blank */
      t[old0] = tile;            /* nuova posizione di tile */
      p0 = new0;
      search(invop(dir), end,end + nodi_sopravissuti);
      if(finito) return;
      p0 = old0;
      t[old0] = 0;
      t[new0] = tile;
    }
  }
  soglia++;
  return;
}


 
/* **************************************************************
   inizializza la ricerca:
   genera top_front e bottom_front
   inizializza stack[];
   calcola (e restituisce) il minimo delle euristiche
   ************************************************************** */
int init_search(void)
{
  void pseudo_qsort(int, int);
  void genera_new(index n, int depth);
  void compute_hvalues(nodo n);
  nodo n;
  index k;
  int i,h,min_h;

  if(max_size>stack_size) {
    fprintf(stderr,"Error, stack_size must be greater than max_size!\n");
    exit(1);
  }

  /* ----- copia la posizione iniziale in nodi[0] ---- */
  for(i=0;i<pr_size;i++)
    n.p[start_t[i]] = i;
  n.invers = -1;
  nodi[0] = n;
  top_begin = 0;
  top_end = 0;

  /* ------ calcola la distanza dal nodo iniziale al nodo finale ---- */
  h=0;
  for(i=1;i<pr_size;i++)  /* calcola la distanza di n dal nodo finale */
    h += mantab[n.p[i]][i];

  /* --------- mette il nodo finale in nodi[bottom_start] ------ */
  bottom_start = top_end+1;
  for(i=0;i<pr_size;i++)
    n.p[i] = i;
  n.invers = -1;
  nodi[bottom_start] = n;
  hvalues[bottom_start] = h; /* memorizza h nella posizione del nodo finale */

  /* ------ genera il fronte bottom ------------ */
  genera_new(bottom_start, bottom_depth);
  bottom_begin = genera_begin;
  bottom_end = genera_end;
  totale_bottom = bottom_end-bottom_begin +1;
  printf("Backward tree size: %d.",bottom_end-bottom_start+1);
  printf("   bottom nodes: %d\n",totale_bottom);
  printf("Nodes generated during bottom search %lu\n",ngen);

  /* ----- copia le euristiche nelle posizioni giuste  ------ */
  for(k=bottom_begin;k<=bottom_end; k++)
    hvalues[k-bottom_begin] = hvalues[k];

  /* ------ inizializza stack[] --------------- */
  for(k=bottom_begin;k<=bottom_end;k++)
    stack[k-bottom_begin] = nodi + k;
    
#if small_front
  if(totale_bottom>small_front) {
    pseudo_qsort(0,totale_bottom-1);
    totale_bottom=small_front;
    printf("Totale nodi nel bottom front ridotto: %d\n",totale_bottom); 
  }
#endif  

  /* ------------- calcola il minimo delle euristiche --------------- */
  min_h = 255;    /* minimo delle euristiche */
  for(i=bottom_end-bottom_begin;i>=0;i--) {
    h = hvalues[i];
    if(min_h > h) min_h = h;      /* calcola il minimo delle h */
  }
  return min_h+top_depth+bottom_depth;  /* limite inferiore al costo */
}




/* ===== variabili globali usate per la gestione della hash table ==== */
ht_index hash1,hash2;         /* output della funzione hash */
ht_index ht_total;            /* numero di nodi presenti nella hash table */
index *ht;                    /* hash table */
/* ==================================================================== */
 
/* ********************************************************************
   genera in maniera breadth-first i successori di nodi[n] fino a
   profondita' depth
   All'uscita della routine il fronte generato occupa le posizioni da
   genera_begin a genera_end
   calcola le distanze euristiche da nodi[0] e le memorizza in hvalues[]
   ******************************************************************** */
void genera_new(index n, int depth)
{
  int new(nodo n, index freep);
  index next;       /* prossimo nodo da espandere */
  int cur_depth;    /* profondita di nodi[next] */
  index last;       /* ultimo nodo in nodi[] a profondita' cur_depth */
  index freep;      /* prima posizione libera in nodi[] */
  nodo padre, figlio;
  int i,k,new0,indice;
  int tile_mossa=-1, dest;  /* tile mossa e sua destinazione */
 
  /* ------------------------ crea la hash table -------------------- */
  ht = (index *) calloc(hash_size, sizeof(index));
  if(ht==NULL) {
    fprintf(stderr,"Not enough memory for the hash table! --genera_new--\n");
    exit(1);
  }
  for(i=0;i<hash_size;i++)  /* --------- inizializza la ht -------- */
    ht[i] = illegal_node;
  ht_total=0;               /* numero di nodi nella ht */
 
  /* --------------- inizializza ----------------- */
  next=n;
  cur_depth=0;
  last=n;
  freep=n+1;
  new(nodi[n],n);  /* mette nodi[n] nella ht */
  /* --------------- genera i nodi --------------- */
  while(cur_depth<depth) {  /* genera nodi di livello cur_depth+1 */
    padre = nodi[next];
    for(i=0;i<4;i++) {
      if(i==padre.invers)
        continue;
      indice = (i<<psize)|padre.p[0];
      new0=nuovapos[indice];  /* nuova posizione del blank */
      if(new0<0)
        continue;                /* direzione non valida */
      ngen++;                    /* incrementa il numero dei nodi generati */
      figlio.p[0] = new0;        /* calcola l'array figlio.p[] */
      for(k=1;k<pr_size;k++) {
        if(padre.p[k] == new0) {
          tile_mossa = k;
          figlio.p[k] = padre.p[0];
        }
        else
          figlio.p[k] = padre.p[k];
      }
      if(new(figlio,freep)) {     /* verifica se e' un nodo nuovo */
        figlio.invers = invop(i);   /* operatore inverso */
        dest = nodi[0].p[tile_mossa];
        hvalues[freep] = hvalues[next] + manhvar[(indice<<psize)|dest];
        nodi[freep++] = figlio;     /* memorizza il figlio */
      }
      if(freep>=max_size) {
        fprintf(stderr,"Array nodi[] is too small! --genera_new--\n");
        exit(1);
      }
    }
    next++;
    if(next>last) {  cur_depth++;  last=freep-1; }
  }
  /* ------------------ operazioni finali ---------------------- */
  free(ht);
  genera_begin=next;
  genera_end=last;
  return;              /* restituisce la posizione dell'ultimo nodo */
}
 
 
 
/* *************************************************************
   questa funzione controlla se il nodo n e' nella ht, se c'e' ritorna 0
   altrimenti ritorna 1 e inserisce il nodo nella ht
   ************************************************************* */
int new(nodo n, index freep)
{
  void hash(nodo);            /* l'output di hash e'in hash1 e hash2 */
  ht_index h1;                /* indice nella ht */
  nodo m;
  int i;
 
  hash(n);
  h1 = hash1 % hash_size;
  while(ht[h1] != illegal_node) {
    m=nodi[ht[h1]];             /* c'e un nodo nella posizione h1 */
    for(i=0;i<pr_size;i++) {
      if(n.p[i] != m.p[i])
        goto sono_diversi;
    }
    return 0;                  /* i nodi sono uguali, esci */
sono_diversi:
    h1 += hash2;
    h1 %= hash_size;
  }
  /* ---- inserisci m nella ht (NB m andra nella posizione nodi[freep]) --- */
  ht[h1] = freep;
  /* ----- controlla che ci sia posto -------- */
  if(++ht_total > hash_size - 200) {
    fprintf(stderr,"Hash table full! --new-- \n");
    exit(1);
  }
  return 1;
}
 
 
/* ****************************************************************
   funzione di hashing (vedere 6.4 di The art of computer programming)
   hash: prende in input il codice di uno stato e restituisce
   in output  0 <= hash1  e 0< hash2
   **************************************************************** */
#define fiinv 40503L
void hash(nodo n)
{
  unsigned char *pp;     /* posizioni delle tessere */
  int i;
 
  pp = n.p;
  hash1 = hash2 = 0;
  /* ---------------------------------------------------------------
     l'efficacia di questa funzione di hash e' tutta da verificare
     confronta anche il file 15.aux
     --------------------------------------------------------------- */
  for(i=3;i<pr_size; i+=4) {
    hash1 ^= (pp[i] + (pp[i-1] << 4)) * fiinv << (i/4);
    hash2 ^= (pp[i-2] + (pp[i-3] << 4)) * fiinv << (i/4);
  }
  hash1 ^= hash2;         /* mescola hash1 con hash2 */
  hash2 |= 1;             /* ottiene  hash2>0 e dispari */
}
 
 
/* *****************************************************************
   alloca la memoria per gli array:
     nodo   nodi[]    (contiene tutti i nodi generati)
     nodopt stack[]   (stack dei nodi attivi durante l'esecuzione di search)
     unsigned char hvalues[] (euristiche dei nodi in stack)
   ***************************************************************** */
void alloc_mem(void)
{
 
  nodi = (nodo *) calloc(max_size, sizeof(nodo));
  stack = (nodopt *) calloc(stack_size, sizeof(nodopt));
  hvalues = (unsigned char *) calloc(stack_size, sizeof(unsigned char));

  if((nodi==NULL)||(stack==NULL)||(hvalues==NULL)) {
    fprintf(stderr,"Not enough memory! --alloc_mem--\n");
    exit(1);
  }
}



/* *************************************************************************
   esegue il sorting con quicksort, ma solo delle prime small_front posizioni 
   ************************************************************************** */
void pseudo_qsort(int p, int r)
{
  // void pseudo_qsort(int, int);
  // int randomint(int n);    /* returns an integer between 0 and n-1 */
  int i,j,k;
  unsigned char x, temph;
  nodopt temps;

  if(p>=small_front)
    return;
  if(p<r) {
    /* ---------- partiziona --------- */
    k = p + randomint(r-p+1);
    temph = hvalues[k]; hvalues[k] = hvalues[p]; hvalues[p] = temph;
    temps = stack[k]; stack[k] = stack[p]; stack[p] = temps;
    x = hvalues[p];   /* valore del pivot */
    i=p-1;
    j=r+1;
    while(1) {
      do {
        j--;
      } while(hvalues[j] > x);
      do {
        i++;
      } while(hvalues[i] < x);
      if(i<j) {
        temph = hvalues[i]; hvalues[i] = hvalues[j]; hvalues[j] = temph;
        temps = stack[i]; stack[i] = stack[j]; stack[j] = temps;
      }
      else
        break;
    }
    pseudo_qsort(p,j);
    pseudo_qsort(j+1,r);
  }
}
