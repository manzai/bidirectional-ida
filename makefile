# set to 0 to remove the per iteration information 
VERBOSE=1
# compilation options
CC=gcc
CFLAGS=-std=c11 -Wall -g -O3
LDLIBS=-lm

# executable files to be created
EXECS=idanew11 ida2 

all: $(EXECS) 

idanew11: idanew11.c 15puzzle.h 24puzzle.h common.h
	$(CC) $(CFLAGS) $< -lm -o $@  -Dverbose=$(VERBOSE)

ida2: ida2.c 15puzzle.h common.h
	$(CC) $(CFLAGS) $< -lm -o $@  -Dverbose=$(VERBOSE)


clean: 
	rm -f $(EXECS)

